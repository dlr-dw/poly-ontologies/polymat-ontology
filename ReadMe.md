
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.12586383.svg)](https://doi.org/10.5281/zenodo.12586383)

# PolyMat ontology
The Polymer Membrane Materials Ontology, PolyMat ontology (pmat) is a domain ontology in polymer membrane research. It provides a common vocabulary for scientists and processes within the domain.
The ontology is applicable to documentation of scientific work and laboratory processes, standardization of terminology in Electronic Laboratory Notebooks (ELNs) and knowledge representation in modelling laboratory processes.

## Summary and documentation
To get familiar with the PolyMat ontology, you can visit its [online documentation](https://w3id.org/polymat/).

## Reused ontologies
* [Relation ontology (RO)](http://obofoundry.org/ontology/ro.html)
* [The PROV Ontology (PROV-O)](https://www.w3.org/TR/2013/REC-prov-o-20130430/)
* [Chemical Entities of Biological Interest (ChEBI) ontology](https://www.ebi.ac.uk/chebi/)
* [Ontology of units of Measure (OM)](http://www.ontology-of-units-of-measure.org/page/om-2)

## Licence
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/).

## Contributors
The PolyMat ontology has following contributors:
* [Marta Dembska](https://orcid.org/0000-0002-8180-1525) (DLR Institute of Data Science)
* [Martin Held](https://orcid.org/0000-0003-1869-463X) (Helmholt-Zentrum Hereon)
* [Sirko Schindler](https://orcid.org/0000-0002-0964-4457) (DLR Institute of Data Science)

## Usage
To explore use of the PolyMat ontology, [competency questions](https://gitlab.com/dlr-dw/poly-ontologies/polymat-ontology/-/blob/main/doc/competency_questions.md) used for ontology engineering and [example data](https://gitlab.com/dlr-dw/poly-ontologies/polymat-ontology/-/blob/main/data/example_data.ttl) are available.

## Evaluation
[Queries](https://gitlab.com/dlr-dw/poly-ontologies/polymat-ontology/-/blob/main/doc/queries.md) used for evaluation of the ontology development process are available for the users.


## Request and comments
To present the contributors with any input on the ontology, please create an issue.
