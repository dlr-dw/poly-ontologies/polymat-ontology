## Competency questions
1. [What polymers were used to fabricate a given membrane?](#1-what-polymers-were-used-to-fabricate-a-given-membrane)
2. [What monomers were used to synthesise a given polymer?](#2-what-monomers-were-used-to-synthesise-a-given-polymer)
3. [How are the monomers arranged in the polymer?](#3-how-are-the-monomers-arranged-in-the-polymer)
4. [What method was used for a given polymer synthesis or membrane fabrication?](#4-what-method-was-used-for-a-given-polymer-synthesis-or-membrane-fabrication)
5. [What is the value of a specific parameter of the method used for the synthesis?](#5-what-is-the-value-of-a-specific-parameter-of-the-method-used-for-the-synthesis)
6. [What is the value of a specific parameter for the preparation before analysis?](#6-what-is-the-value-of-a-specific-parameter-for-the-preparation-before-analysis)
7. [What is the type of a membrane?](#7-what-is-the-type-of-a-membrane)
8. [How are different membranes arranged in a module?](#8-how-are-different-membranes-arranged-in-a-module)
9. [Who were the persons performing a given experiment?](#9-who-were-the-persons-performing-a-given-experiment)
10. [What equipment was used in a given experiment?](#10-what-equipment-was-used-in-a-given-experiment)
11. [What characteristics of polymers or membranes are recorded?](#11-what-characteristics-of-polymers-or-membranes-are-recorded)
12. [What (physical, chemical) calculated characteristics are derived from a measured characteristic?](#12-what-physical-chemical-calculated-characteristics-are-derived-from-a-measured-characteristic)
13. [To measure a given characteristic of a membrane or polymer, what method was used?](#13-to-measure-a-given-characteristic-of-a-membrane-or-polymer-what-method-was-used)
14. [To calculate a given characteristic of a membrane or polymer, what method was used?](#14-to-calculate-a-given-characteristic-of-a-membrane-or-polymer-what-method-was-used)
15. [Where are the results of a given calculation stored?](#15-where-are-the-results-of-a-given-calculation-stored)
16. [What software was used to perform an experiment or calculation?](#16-what-software-was-used-to-perform-an-experiment-or-calculation)
17. [What computational models were used to perform the experiment?](#17-what-computational-models-were-used-to-perform-the-experiment) 
18. [When was an experiment request submitted?](#18-when-was-an-experiment-request-submitted)
19. [When was a synthesis or fabrication request submitted?](#19-when-was-a-synthesis-or-fabrication-request-submitted)

## Examples of use
### 1. What polymers were used to fabricate a given membrane? 
#### Example 1.1
```mermaid
flowchart LR
  subgraph " "
  Acr[PTFE]
  end

  subgraph " "
  ID[MHp201203$]
  end

  subgraph " "
  casnr[9002-84-0]
  end

  subgraph " "
  iupac["Poly(1,1,2,2-tetrafluoroethylene)"]
  end

  subgraph "Homopolymer, Poly(tetrafluoroethylene)"
  Polymer(polytetrafluoroethylene1)
  Polymer -->|has acronym| Acr
  Polymer -->|has product ID| ID
  Polymer -->|has CAS nr| casnr
  Polymer -->|has IUPAC name| iupac
  end

  subgraph "Membrane for liquids, FlatSheetMembrane"
  Membrane(polytetrafluoroethyleneMembrane5) -->|contains| Polymer
end
```

### 2. What monomers were used to synthesise a given polymer? 
### Example 2.1
```mermaid
flowchart LR
  subgraph "Monomer, Tetrafluoroethylene"
  Monomer(tetrafluoroethylene2)
  end

  subgraph "Homopolymer, Polytetrafluoroethylene "
  Polymer(polytetrafluoroethylene1) -->|contains| Monomer
end
```

### 3. How are the monomers arranged in the polymer?
### Example 3.1
```mermaid
flowchart LR
  subgraph "BlockCopolymer"
  Polymer(butylRubber1)
  end
```


### 4. What method was used for a given polymer synthesis or membrane fabrication? 
### Example 4.1
```mermaid
flowchart LR
  subgraph "BlockCopolymer, Isobutylene-isoprene copolymer"
  Polymer(butylRubber1)
  end

  subgraph "Cationic polymerisation"
  Method(synthesis32)
  end

  subgraph "Experiment"
  Experiment43(experiment43) -->|used| Method
  Experiment43 -->|generated| Polymer
end
```

### Example 4.2
```mermaid
flowchart LR
  subgraph "Flatsheet membrane, Membrane for liquids"
  Membrane(polytetrafluoroethyleneMembrane5)
  end

  subgraph "Flatsheet membrane casting"
  Method(fabrication56)
  end

  subgraph "Experiment"
  Experiment55(experiment55) -->|used| Method
  Experiment55 -->|generated| Membrane
end
```

### 5. What is the value of a specific parameter of the method used for the synthesis?
### Example 5.1
```mermaid
flowchart LR
  subgraph " "
  100["-100"]
  end

  subgraph "Unit"
  C("degreeCelsius")
  end

  subgraph "Measure"
  neg100(_negative100C) -->|has numerical value| 100
  neg100 -->|has unit| C
  end

  subgraph "Temperature"
  Quantity(polymerisationTemperature8) -->|has value| neg100
  end

  subgraph "BlockCopolymer, Isobutylene-isoprene copolymer"
  Polymer(butylRubber1)
  end

  subgraph "Cationic polymerisation"
  Method(synthesis32) -->|has participant| Quantity
  end

  subgraph "Experiment"
  Experiment43(experiment43) -->|used| Method
  Experiment43 -->|generated| Polymer
end
```

### 6. What is the value of a specific parameter for the preparation before analysis?
#### Example 6.1
```mermaid
flowchart LR
  subgraph " "
  3[3]
  end

  subgraph "Unit"
  nm("nanometre")
  end

  subgraph "Measure1"
  3nm( _3nm) -->|has numerical value| 3
  3nm -->|has unit| nm
  end

  subgraph "Pt Thickness"
  Quantity1(PtThickness26) -->|has value| 3nm
  end

  subgraph "Flat sheet membrane, Membrane for liquids"
  Membrane(polytetrafluoroethyleneMembrane5)
  end

  subgraph "Sample preparation"
  Method1(samplePreparation100) -->|has participant| Quantity1
  end

  subgraph "Experiment1"
  Experiment30(experiment30) -->|used| Method1
  Experiment30 -->|generated| Membrane
  end

  subgraph " "
  50[50]
  end

  subgraph "Measure2"
  50nm(_50nm) -->|has numerical value| 50
  50nm -->|has unit| nm
  end

  subgraph "Mean Pore Diameter"
  Quantity2(meanPoreDiamer168) -->|has value| 3nm
  end

  subgraph "Imaging SEM"
  Method2( analysis30) -->|has participant| Quantity2
  end

  subgraph "Experiment2"
  Experiment138(experiment138) -->|used| Method2
  Experiment138 -->|used| Membrane
  Experiment138 -->|realizable has basis in| Experiment30
end
```

### 7. What is the type of a membrane?
#### Example 7.1
```mermaid
flowchart LR
  subgraph "Membrane for liquids, Flatsheet membrane"
  Membrane(polytetrafluoroethyleneMembrane5)
  end
```

### 8. How are different membranes arranged in a module?
#### Example 8.1
```mermaid
flowchart LR
  subgraph "Flat sheet envelope module"
  Module(k100Module2)
  end
```

### 9. Who were the persons performing a given experiment?
#### Example 9.1
```mermaid
flowchart LR
  subgraph "User"
  Role(laboratoryTechnician)
  end
  subgraph "Imaging SEM"
  Method(analysis40)
  end

  subgraph "Person"
  Ilona(ilona)
  end

  subgraph "Association"
  blankAssociation1(" ") -->|had role| Role
  blankAssociation1 -->|agent| Ilona
  end

  subgraph "Experiment"
  Experiment31(experiment31) -->|qualified association| blankAssociation1
  Experiment31 -->|was associated| Ilona
  Experiment31 -->|used| Method

end
```

### 10. What equipment was used in a given experiment?
#### Example 10.1
```mermaid
flowchart LR
  subgraph "Imaging SEM"
  Method( analysis30)
  end

  subgraph "Scanning electron microscope (SEM)"
  Device(scanningElectronMicroscopeMerlin)
  end

  subgraph "Experiment"
  Experiment138(experiment138) -->|used| Method
  Experiment138 -->|used| Device
end
```

### 11. What characteristics of polymers or membranes are recorded?
#### Example 11.1
```mermaid
flowchart LR

  subgraph " "
  333[333]
  end

  subgraph "Unit"
  MPa("megapascal")
  end

  subgraph "Measure"
  333MPa(_333MPa) -->|has numerical value| 333
  333MPa -->|has unit| MPa
  end

  subgraph "Ultimate tensile strength"
  Quantity(ultimateTensileStrength23) -->|has value| 333MPa
  end

  subgraph "Homopolymer, Poly(tetrafluoroethylene)"
  Polymer(polytetrafluoroethylene1) -->|has characteristic| Quantity 
end
```

#### Example 11.2
```mermaid
flowchart LR

  subgraph " "
  300[300]
  end

  subgraph "Unit"
  um("micrometre")
  end

  subgraph "Measure"
  300um( _300microm) -->|has numerical value| 300
  300um -->|has unit| um
  end

  subgraph "Thickness"
  Quantity( thickness45) -->|has value| 300um
  end

  subgraph "Flatsheet membrane, Membrane for liquids"
  Membrane(polytetrafluoroethyleneMembrane5) -->|has characteristic| Quantity 
end
```


### 12. What (physical, chemical) calculated characteristics are derived from a measured characteristic?
#### Example 12.1
```mermaid
flowchart LR
  subgraph "Homopolymer, Poly(tetrafluoroethylene)"
  Polymer(polytetrafluoroethylene1)
  end

  subgraph "Tensile test"
  Method(tensileTest1001)
  end


  subgraph "Experiment"
  experiment530(experiment530)  -->|used| Polymer
  experiment530 -->|used| Method
  end

  subgraph " "
  2[2]
  end

  subgraph "Unit"
  GPa("gigapascal")
  end

  subgraph "Measure"
  2GPa(_2GPa) -->|has numerical value| 2
  2GPa(_2GPa) -->|has unit| GPa
  end

  subgraph "Quantity" 
  stress78 -->|output of| experiment530
  stress78 -->|characteristic of| Polymer
  modulusOfElasticity41(modulusOfElasticity41) -->|has value| 2GPa
  modulusOfElasticity41 -->|realizable has basis in| stress78
  modulusOfElasticity41 -->|characteristic of| Polymer
  end

  subgraph "Fitting"
  fitting31(fitting31) -->|has output| modulusOfElasticity41
end
```


### 13. To measure a given characteristic of a membrane or polymer, what method was used?
#### Example 13.1
```mermaid
flowchart LR
  subgraph "Homopolymer, Poly(tetrafluoroethylene)"
  Polymer(polytetrafluoroethylene1)
  end

  subgraph "Tensile test"
  Method(tensileTest1001)
  end


  subgraph "Experiment"
  experiment530(experiment530)  -->|used| Polymer
  experiment530 -->|used| Method
  end

  subgraph " "
  333[333]
  end

  subgraph "Unit"
  MPa("megapascal")
  end

  subgraph "Measure"
  333MPa(_333MPa) -->|has numerical value| 333
  333MPa -->|has unit| MPa
  end

  subgraph "Quantity" 
  ultimateTensileStrength23(ultimateTensileStrength23) -->|output of| experiment530
  ultimateTensileStrength23 -->|characteristic of| Polymer
  ultimateTensileStrength23 -->|has value| 333MPa
end
```

### 14. To calculate a given characteristic of a membrane or polymer, what method was used?
#### Example 14.1
```mermaid
flowchart LR
  subgraph "Homopolymer, Poly(tetrafluoroethylene)"
  Polymer(polytetrafluoroethylene1)
  end

  subgraph "Fitting"
  fitting31(fitting31)
  end

  subgraph " "
  2[2]
  end

  subgraph "Unit"
  GPa("gigapascal")
  end

  subgraph "Measure"
  2GPa(_2GPa) -->|has numerical value| 2
  2GPa -->|has unit| GPa
  end

  subgraph "Quantity" 
  modulusOfElasticity41(modulusOfElasticity413) -->|output of| fitting31
  modulusOfElasticity41 -->|characteristic of| Polymer
  modulusOfElasticity41 -->|has value| 2GPa
end
```

### 15. Where are the results of a given calculation stored?
#### Example 15.1
```mermaid
flowchart LR
  subgraph "Calculation"
  calculation3545445(calculation3545445)
  end

  subgraph "Output data"
  outputData24354(outputData24354)
  end

  subgraph "Result"
  result5656(result5656)
  end
  
  subgraph " "
  Path["PM_REM/Schmidt/S20017"]
  end


  subgraph "Digital data"
  dataset202302130023(dataset202302130023) -->|output of| calculation3545445
  dataset202302130023 -->|has characteristic| outputData24354
  dataset202302130023 -->|has characteristic| result5656
  dataset202302130023 -->|has path| Path
  end

  subgraph "Data repository"
  PMserver("PM server") -->|location of| dataset202302130023
end

```

### 16. What software was used to perform an experiment or calculation?
#### Example 16.1
```mermaid
flowchart LR
  subgraph "Author"
  Role(authorOfInhouseSoftware3)
  end

  subgraph "Person"
  Carsten(carsten)
  end

  subgraph "Attribution"
  blank_attribution34(" ") -->|had role| Role
  blank_attribution34 -->|agent| Carsten
  end 

  subgraph "Inhouse software"
  Software(MembraneSandwichExe) -->|qualified attribution| blank_attribution34
  Software(MembraneSandwichExe) -->|was attributed to| Carsten
  end

  subgraph "Experiment"
  Experiment40(experiment40)
  Experiment40 -->|used| Software
end
```

### 17. What computational models were used to perform the experiment? 
#### Example 17.1
```mermaid
flowchart LR
  subgraph "Computational model"
  Model(AspenSimulation99)
  end 

  subgraph "Experiment"
  Experiment40(experiment40) -->|used| Model
  Experiment40 -->|has model| Model
  Model-->|has role in modeling| Experiment40
end
```

### 18. When was an experiment request submitted?
#### Example 18.1
```mermaid
flowchart LR
  subgraph "  "
  dateTime["2023-01-05T09:14:56+01:00"]
  end

  subgraph " "
  OrderNr[2023000237]
  end

  subgraph "Experiment request submission "
  requestSubmission2023000237(requestSubmission2023000237) -->|submission started at| dateTime
  requestSubmission2023000237 -->|hasOrderNumber| OrderNr
  end 

  subgraph "Experiment"
  requestSubmission2023000237 -->|is basis for realizable| Experiment31(experiment31)
end
```

### 19. When was a synthesis or fabrication request submitted?
#### Example 19.1
```mermaid
flowchart LR
  subgraph "  "
  dateTime["2023-01-05T09:14:56+01:00"]
  end

  subgraph " "
  Type[fabrication]
  end

  
  subgraph " "
  OrderNr[2023000237]
  end

  subgraph "Experiment request submission "
  requestSubmission2023000237(requestSubmission2023000237) -->|submission started at| dateTime
  requestSubmission2023000237 --> |has request submission type| Type
  requestSubmission2023000237 -->|hasOrderNumber| OrderNr
  end 

  subgraph "Experiment"
  requestSubmission2023000237 -->|is basis for realizable| Experiment31(experiment31)
end
```

#### Example 19.2
```mermaid
flowchart LR
  subgraph "  "
  dateTime["2023-01-09T11:27:13+01:00"]
  end

  subgraph " "
  Type[synthesis]
  end

  subgraph " "
  OrderNr[2023000240]
  end

  subgraph "Experiment request submission "
  requestSubmission2023000240(requestSubmission2023000240) -->|submission started at| dateTime
  requestSubmission2023000240 -->|has request submission type| Type
  requestSubmission2023000240 -->|hasOrderNumber| OrderNr
  end 

  subgraph "Experiment"
  requestSubmission2023000240 -->|is basis for realizable| Experiment70(experiment70)
end
```




